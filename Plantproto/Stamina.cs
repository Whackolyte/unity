﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Stamina : MonoBehaviour
{
	public Image innerbar;
	public Text textstam
	public float max_stam = 100f;
	public float cur_stam = 10f;

	void Start () 
	{
		cur_stam = max_stam;
	}

	void UpdateStam()
	{
		float ratio = cur_stam;
		innerbar.rectTransform.localScale = new Vector3 (ratio, 1, 1);
		textstam.text = (ratio * 100).ToString () + '%';
	}
	void OnMouseDown() {
		cur_stam =cur_stam-1;
		Debug.Log("done")
	}
}