﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantClass : MonoBehaviour {

		public class Plant
		{
		public float Growtime;
		//how long it takes the plant to grow
			public int Health;
		//how much healthe the plant has

		public float MaxTime;
		
		//nicknames for interger use
		public Plant(float Gro, int Hp)
			{
				Growtime = Gro;
				Health = Hp;
				MaxTime=Gro;
			}
				

			// Constructor, values for Plant
			public Plant ()
			{
				Growtime = 10;
				Health = 10;
				MaxTime=10;
			}
		}


		// Creating an Instance (an Object) of the plant class
	public Plant testPlant = new Plant(50, 20);

	public Plant testPlant2 = new Plant(20, 10);


	void Start() {
		//gameObject.renderer.material.color = Color.green;

	}
	
	
	void Update ()
	{
		if (testPlant2.Growtime > 0)
		{
			//Debug.Log (testPlant2.Growtime + "Grown!");
			testPlant2.Growtime -= UnityEngine.Time.deltaTime;
			if (testPlant2.Growtime <= 0)
			{
				Debug.Log("Plant 2 grown!");
			}
		}
		if (Input.GetKeyDown(KeyCode.A))
		{

			//testPlant.Health -= 1;
			Debug.Log (testPlant2.Growtime + "time left to grow"); 
			//Debug.Log (testPlant2.Health);
		}
		Renderer rend = GetComponent<Renderer>();
		rend.material.color = new Color(0,5*(1-(testPlant2.Growtime/testPlant2.MaxTime)),0,1);
		
	}
}