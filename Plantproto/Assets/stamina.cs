﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class stamina: MonoBehaviour
{
	public Image Visual_Gauge;
	public Text Text_Stamina;
	public float period = 0f;
	float max_stam = 10f;
	public static float cur_stam = 0f;

	void Start () 
	{
		max_stam = 10f;
		cur_stam = 0f;
		float ratio = cur_stam / max_stam;

	//	Visual_Gauge.transform.localScale = new Vector3 (ratio, 4, 1);
	//	Text_Stamina.text = ("testing text").ToString () + '%';
	//	irrelevent code, can be removed
	}

	void Update ()
	{
		if (period > 5f && (cur_stam < max_stam))
		{
			cur_stam += 1;
			Debug.Log ("stamina = "+ cur_stam);
			period = 0f;
		}
		float ratio = cur_stam/max_stam;
		Visual_Gauge.fillAmount = (ratio);
		Text_Stamina.text = (ratio * 100).ToString () + '%';
		//ratio never works, will display cur_stam and max_stam
		period += UnityEngine.Time.deltaTime;
	}
}