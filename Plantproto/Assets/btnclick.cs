﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class btnclick : MonoBehaviour {

	//public Button ButtonObj;

	// Use this for initialization
	void Start () {
		Button btn = gameObject.GetComponent<Button> ();
		btn.onClick.AddListener (TaskClick);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void TaskClick() {
		if (stamina.cur_stam > 0) {
			stamina.cur_stam -= 1;
			Debug.Log ("Button pressed.  Stamina = " + stamina.cur_stam);
		}

	}

}

